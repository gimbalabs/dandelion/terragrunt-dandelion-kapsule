kubernetes_version = "1.21.1"
cni_plugin = "flannel"
auto_upgrade = {
  enable = false
  maintenance_window_day  = "Sunday"
  maintenance_window_start_hour = 0
}

# pool names must follow ${NETWORK}_${ROLE} for node labeling to work
node_pools = {
  mainnet_db_main = {
    node_type   = "GP1-XS"
    size        = 1
    max_size    = 1
    min_size    = 1
    autoscaling = false
    autohealing = true
    wait_for_pool_ready = true
  }
  mainnet_cardano_graphql = {
    node_type   = "DEV1_L"
    size        = 1
    max_size    = 1
    min_size    = 1
    autoscaling = false
    autohealing = false
    wait_for_pool_ready = true
  }
  mainnet_cardano_node = {
    node_type   = "GP1-XS"
    size        = 1
    max_size    = 1
    min_size    = 1
    autoscaling = false
    autohealing = false
    wait_for_pool_ready = true
  }
  testnet_single_node = {
    node_type   = "DEV1_L"
    size        = 1
    max_size    = 1
    min_size    = 1
    autoscaling = false
    autohealing = false
    wait_for_pool_ready = true
  }
}
