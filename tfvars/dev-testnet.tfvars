kubernetes_version = "1.21.1"
cni_plugin = "flannel"
auto_upgrade = {
  enable = false
  maintenance_window_day  = "Sunday"
  maintenance_window_start_hour = 0
}

node_pools = {
  main = {
    tags        = ["main"]
    node_type   = "DEV1_L"
    size        = 1
    max_size    = 1
    min_size    = 1
    autoscaling = false
    autohealing = false
  }
}
