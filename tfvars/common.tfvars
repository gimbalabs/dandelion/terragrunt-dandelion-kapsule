# Match node names with the keys for the arrays

k8s_pools_tags = {
  testnet = {
    single_node = [
        "k8s.dandelion.link/cardano-network=testnet",
        "k8s.dandelion.link/role=single-node"
    ]
  }
  mainnet = {
    db_main = [
      "k8s.dandelion.link/cardano-network=mainnet",
      "k8s.dandelion.link/role=db-main"
    ]
    db_ro = [
      "k8s.dandelion.link/cardano-network=mainnet",
      "k8s.dandelion.link/role=db-ro"
    ]
    cardano_graphql = [
      "k8s.dandelion.link/cardano-network=mainnet",
      "k8s.dandelion.link/role=cardano-graphql"
    ]
    cardano_node = [
      "k8s.dandelion.link/cardano-network=mainnet",
      "k8s.dandelion.link/role=cardano-node"
    ]
  }
}
