# Requirements

* tfenv
* tgenv
* terragrunt (just `tgenv install` and it will use `.terragrunt-version` file)
* terraform (just `tfenv install` and it will use `.terraform-version` file)
* argocd cli
* htpasswd (for argocd's password bcrypt hashing)

# Deploy

* Setup your environment:
```
ENVIRONMENT_NAME=dev
cp -a .env.tpl .env.${ENVIRONMENT_NAME}
# set your credentials
```
* Plan+Deploy
```
ENVIRONMENT_NAME=dev
source .env.${ENVIRONMENT_NAME}
terragrunt run-all init -reconfigure # this might give you an error complaining about an AWS host; just ignore it and wait, it's a bug
terragrunt run-all plan
terragrunt run-all apply
```
