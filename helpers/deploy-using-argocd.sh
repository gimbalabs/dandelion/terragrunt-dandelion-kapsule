#!/bin/bash
set -x

source ../../../.env.${ENVIRONMENT_NAME}

kubectl get ns argocd 
# if namespace exits we assume, everything is already deployed
if [ $? -ne 0 ]
then
  test -z ${KUSTOMIZE_DANDELION_REPO} && KUSTOMIZE_DANDELION_REPO="https://gitlab.com/gimbalabs/dandelion/kustomize-dandelion.git"
  test -z ${KUSTOMIZE_DANDELION_COMMIT} && KUSTOMIZE_DANDELION_COMMIT="update/cardano-db-sync-v10"
  test -z ${ARGOCD_PASSWORD} && ARGOCD_PASSWORD=CH4NG3@M3

  DEPLOY_TIMESTAMP=$(date +%s)
  
  git clone ${KUSTOMIZE_DANDELION_REPO} tg-kustomize-dandelion-${DEPLOY_TIMESTAMP}
  cd tg-kustomize-dandelion-${DEPLOY_TIMESTAMP}/argocd-bootstrap
  
  git checkout ${KUSTOMIZE_DANDELION_COMMIT}
  
  APP_PROVIDER=scaleway
  APP_NETWORK=mainnet
  ARGOCD_HASHED_PASSWORD=$(htpasswd -nbBC 10 null $ARGOCD_PASSWORD | sed 's|null:\(.*\)|\1|g')
  
  helm dependency build
  helm dependency update
  
  helm upgrade \
      --create-namespace \
      --namespace argocd \
      --install argocd \
      --set "argo-cd.configs.secret.argocdServerAdminPassword=${ARGOCD_HASHED_PASSWORD}" \
      --set "argo-cd.configs.secret.argocdServerAdminPasswordMtime=$(date +%FT%T%Z)" \
      --set "dandelion.multiNode=${DANDELION_MULTINODE}" \
      --set "dandelion.db.adminPassword=${DANDELION_DB_ADMINPASSWORD}" \
      --set "dandelion.db.roPassword=${DANDELION_DB_ROPASSWORD}" \
      -f values-${APP_PROVIDER}-${APP_NETWORK}.yaml \
      .

  APP_NETWORK=testnet
  APP_SUFFIX=-v10-0-1
  APP_NAME=dandelion-${APP_NETWORK}${APP_SUFFIX}
  APP_REVISION=${KUSTOMIZE_DANDELION_COMMIT}
  VALUES_FILE=values-${APP_PROVIDER}-${APP_NETWORK}.yaml
  
  while [ -z "$POD_NAME" ]
  do
    POD_NAME=$(kubectl get pod -n argocd --selector 'app.kubernetes.io/name=argocd-server' --template='{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
    sleep 0.5
  done

  RANDOM_PORT=$(shuf -i 1205-60000 -n1)
  kubectl port-forward -n argocd ${POD_NAME} ${RANDOM_PORT}:8080 &

  argocd login localhost:${RANDOM_PORT} --username admin --password $ARGOCD_PASSWORD

  argocd app create \
    ${APP_NAME} \
    --project default \
    --repo https://gitlab.com/gimbalabs/dandelion/kustomize-dandelion.git \
    --revision ${APP_REVISION} \
    --path applications/main-app \
    --helm-set git.targetRevision=${APP_REVISION} \
    --helm-set dandelion.multiNode=${DANDELION_MULTINODE} \
    --helm-set dandelion.db.adminPassword=${DANDELION_DB_ADMINPASSWORD} \
    --helm-set dandelion.db.roPassword=${DANDELION_DB_ROPASSWORD} \
    --values-literal-file ${VALUES_FILE} \
    --sync-policy automated \
    --sync-option Prune=true \
    --sync-option selfHeal=true \
    --sync-option ApplyOutOfSyncOnly=true \
    --dest-server https://kubernetes.default.svc

  pkill -9 -f kubectl.*port-forward.*${RANDOM_PORT}
  
fi 
