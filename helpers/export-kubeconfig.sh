#!/bin/bash

terragrunt output -no-color kubeconfig_file | grep -v "INFO\|ERROR\|EOT"
