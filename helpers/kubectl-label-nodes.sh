#!/bin/bash

NETWORKS="testnet mainnet"
TERRAGRUNT_MAIN_OUTPUTS_FILE=".k8s_pool_tags.json"
TERRAGRUNT_KAPSULE_OUTPUTS_FILE="../../../../.kapsule.output.json"

terragrunt output -no-color -json > ${TERRAGRUNT_MAIN_OUTPUTS_FILE}

jq -r '.k8s_pools_tags.value | keys[]' ${TERRAGRUNT_MAIN_OUTPUTS_FILE} | while read network
do
  jq -r --arg network ${network} '.k8s_pools_tags.value | .[$network] | keys[]' ${TERRAGRUNT_MAIN_OUTPUTS_FILE} | while read pool_name
  do
    jq -r --arg nodeName ${network}_${pool_name} '.node_pools.value | .[$nodeName] | .[] | .nodes[].name' ${TERRAGRUNT_KAPSULE_OUTPUTS_FILE} 2>/dev/null| while read node_name
    do
      LABELS=$(jq -r --arg network ${network} --arg poolName ${pool_name} '.k8s_pools_tags.value | .[$network] | .[$poolName] | .[]' ${TERRAGRUNT_MAIN_OUTPUTS_FILE} | xargs echo)
      kubectl label nodes ${node_name} ${LABELS} --overwrite
    done
  done
done


