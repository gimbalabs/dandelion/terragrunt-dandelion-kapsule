# this just sets an order in case we have many depends
dependencies {
  paths = ["tg-modules//kapsule"]
}

dependency "kapsule" {
  skip_outputs = true
  config_path = "tg-modules//kapsule"
}

#inputs = {
#  node_pools = dependency.kapsule.outputs.node_pools
#}

terraform {

  source = "tg-modules//main"

  extra_arguments "custom_vars" {
    commands = [
      "apply",
      "plan",
      "import",
      "push",
      "refresh"
    ]

    arguments = [
      "-var-file=${get_terragrunt_dir()}/tfvars/common.tfvars",
      "-var-file=${get_terragrunt_dir()}/tfvars/${get_env("ENVIRONMENT_NAME", "development")}.tfvars"
    ]
  }

  after_hook "kubectl-label-nodes" {
    commands     = ["apply"]
    execute      = ["bash", "-c", "export KUBECONFIG=${get_env("KUBECONFIG", "/tmp/kubeconfig")}; bash helpers/kubectl-label-nodes.sh"]
    run_on_error = false
  }

#   after_hook "after_hook" {
#      commands     = ["apply"]
#      execute      = ["bash", "-xc", "export KUBECONFIG=${get_env("KUBECONFIG", "/tmp/kubeconfig")}; export ENVIRONMENT_NAME=${get_env("ENVIRONMENT_NAME", "development")}; ./helpers/deploy-using-argocd.sh"]
#      run_on_error = false
#  }


}
