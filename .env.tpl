# Scaleway credentials
export SCW_ACCESS_KEY="CHANGEME"
export SCW_SECRET_KEY="CHANGEME"
export SCW_PROJECT_ID="CHANGEME"
export SCW_ZONE="fr-par-1" 
export SCW_REGION="fr-par"
## workaround for tg
export AWS_ACCESS_KEY_ID=${SCW_ACCESS_KEY}
export AWS_SECRET_ACCESS_KEY=${SCW_SECRET_KEY}
# Cloudflare credentials
export CF_EMAIL="CHANGE@ME.COM"
export CF_API_KEY="CHANGEME"
# generic/env related
export ENVIRONMENT_NAME=production
export ENVIRONMENT_SHORT_NAME=pro
export PROJECT_NAME=dandelion
export DOMAIN=dandelion.link
export KUBECONFIG=$HOME/.kube/kubeconfig_${ENVIRONMENT_NAME}-${PROJECT_NAME}.yaml
# argo/k8s deploy related
export DANDELION_MULTINODE=true
export DANDELION_DB_ADMINPASSWORD="r@nd0m1z3droot"
export DANDELION_DB_ROPASSWORD="ror@nd0m1z3d"
export ARGOCD_PASSWORD="ch4ng3@M3"
