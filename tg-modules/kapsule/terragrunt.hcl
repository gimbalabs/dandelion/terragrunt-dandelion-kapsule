locals {
  ENVIRONMENT_NAME = get_env("ENVIRONMENT_NAME", "development")
  ENVIRONMENT_SHORT_NAME = get_env("ENVIRONMENT_SHORT_NAME", "dev")
  PROJECT_NAME = get_env("PROJECT_NAME", "default")
}

generate "provider" {
  path = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
provider "scaleway" {
   // organization_id = "${get_env("SCW_ORGANIZATION_ID", "")}"
  access_key      = "${get_env("SCW_ACCESS_KEY", "")}"
  secret_key      = "${get_env("SCW_SECRET_KEY", "")}"
  zone            = "${get_env("SCW_ZONE", "fr-par-1")}"
  region          = "${get_env("SCW_REGION", "fr-par")}"
}
EOF
}

remote_state {
  backend = "s3" 
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite"
  }
  config = {
    bucket                      = "${get_env("ENVIRONMENT_SHORT_NAME", "dev")}-${get_env("PROJECT_NAME", "default")}-tfstate-backend"
    key                         = "${get_env("ENVIRONMENT_SHORT_NAME", "dev")}-${get_env("PROJECT_NAME", "default")}.tfstate"
    region                      = get_env("SCW_REGION", "fr-par")
    endpoint                    = get_env("SCW_S3_ENDPOINT", "https://s3.fr-par.scw.cloud")
    access_key                  = get_env("SCW_ACCESS_KEY", "")
    secret_key                  = get_env("SCW_SECRET_KEY", "")
    skip_credentials_validation = true
    skip_region_validation      = true
  }
}

inputs = {
  environment_name       = get_env("ENVIRONMENT_NAME", "development")
  environment_short_name = get_env("ENVIRONMENT_SHORT_NAME", "dev")
  project_name           = get_env("PROJECT_NAME", "default")
  cluster_name           = "${get_env("ENVIRONMENT_SHORT_NAME", "dev")}-${get_env("PROJECT_NAME", "default")}"
  cluster_description    = "${get_env("ENVIRONMENT_SHORT_NAME", "dev")}-${get_env("PROJECT_NAME", "default")}"
  region                 = get_env("SCW_REGION", "fr-par")
  project_id             = get_env("SCW_PROJECT_ID", "")
  //tags                   = [
  //  "Terraform   = true",
  //  "Terragrunt  = true",
  //  "Environment = get_env("ENVIRONMENT_NAME", "development")",
  //  "Project     = get_env("PROJECT_NAME", "default")",
  //  "Project_Id  = get_env("SW_PROJECT_ID", "default")"
  //]

}

terraform {

  source = "git@github.com:particuleio/terraform-scaleway-kapsule.git////?ref=v3.0.2"

  extra_arguments "custom_vars" {
    commands = [
      "apply",
      "plan",
      "import",
      "push",
      "refresh"
    ]

    arguments = [
      "-var-file=${get_terragrunt_dir()}/../../tfvars/common.tfvars",
      "-var-file=${get_terragrunt_dir()}/../../tfvars/${get_env("ENVIRONMENT_NAME", "development")}.tfvars"
    ]
  }

  after_hook "export-kubeconfig" {
    commands     = ["apply"]
    execute      = ["bash", "-c", "bash ${get_terragrunt_dir()}/../../helpers/export-kubeconfig.sh > ${get_env("KUBECONFIG", "/tmp/kubeconfig")}"]
    run_on_error = false
  }

  after_hook "export-outputs-json" {
    commands     = ["apply"]
    execute      = ["bash", "-c", "bash ${get_terragrunt_dir()}/../../helpers/export-outputs-json.sh > ${get_terragrunt_dir()}/../../.kapsule.output.json"]
    run_on_error = false
  }

}
