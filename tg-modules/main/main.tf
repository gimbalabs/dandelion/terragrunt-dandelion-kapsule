#resource "null_resource" "kubectl-label-nodes" {
#
#  provisioner "local-exec" {
#    command = "bash helpers/kubectl-label-nodes.sh"
#  }
#}
#
#variable "tags" {
#  default = []
#}

variable "k8s_pools_tags" {
  default = {}
}

output "k8s_pools_tags" {
  value = var.k8s_pools_tags
}
